// console.log('hello');

// JSON is a popuar data format for applications
//to communicate with each other
//JSON may look like an object but it is a string

/*

	JSON syntax:
	{

		"key1": "valueA",
		"key2": "valueB"
	}
*/
let sample1 = `
	{
	"age": "Katniss Everdeen",
	"age": "20",
	"address": {
	"city": "Kansas City",
			"state": "Kansas"
				}
	}
`;
console.log(sample1);

//turn JSON into JS object

console.log(JSON.parse(sample1));

let sampleArr = `
	[
		{
			"email": "jasonNewsted@metallica.com",
			"password": "bestBassist",
			"isAdmin": false
		},
		{
			"email": "larsUlrich@drums.com",
			"password": "worstDrumTuning",
			"isAdmin": true
		}
	]
`;

console.log(sampleArr);

let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);

console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

//database (JSON) => server/API => (JSON to JS object to process) => sent to JSON => client




